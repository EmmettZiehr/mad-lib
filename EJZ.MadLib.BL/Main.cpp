#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <fstream>

/*
* Student: Emmett Ziehr.
* Assignment: Lab Exercise 3.
* Professor: Ryan Appel.
* Due date: 3/13/2021.
*/

void printToConsole(std::string *conIO, std::ostream &os);

int main()
{
	char save;
	std::string conIO[19];

	std::cout << "Please enter the following questions:" << std::endl << std::endl;
	 
	for (int i = 0; i < 19; i++)
	{
		if (i == 0) { std::cout << "Enter a number:" << std::endl; }
		else if (i == 1) { std::cout << "Enter a plural noun:" << std::endl; }
		else if (i == 2) { std::cout << "Enter a verb:" << std::endl; }
		else if (i == 3) { std::cout << "Enter an adjective:" << std::endl; }
		else if (i == 4) { std::cout << "Enter a number:" << std::endl; }
		else if (i == 5) { std::cout << "Enter a place:" << std::endl; }
		else if (i == 6) { std::cout << "Enter a first name:" << std::endl; }
		else if (i == 7) { std::cout << "Enter a first name:" << std::endl; }
		else if (i == 8) { std::cout << "Enter a verb ending in -ed:" << std::endl; }
		else if (i == 9) { std::cout << "Enter a noun:" << std::endl; }
		else if (i == 10) { std::cout << "Enter a verb ending in -ed:" << std::endl; }
		else if (i == 11) { std::cout << "Enter a number:" << std::endl; }
		else if (i == 12) { std::cout << "Enter an adjective:" << std::endl; }
		else if (i == 13) { std::cout << "Enter an adjective:" << std::endl; }
		else if (i == 14) { std::cout << "Enter a verb ending in -ed:" << std::endl; }
		else if (i == 15) { std::cout << "Enter a noun:" << std::endl; }
		else if (i == 16) { std::cout << "Enter a verb ending in -ed:" << std::endl; }
		else if (i == 17) { std::cout << "Enter an adverb:" << std::endl; }
		else if (i == 18) { std::cout << "Enter a number:" << std::endl; }

		getline(std::cin, conIO[i]);
	}

	printToConsole(conIO, std::cout);

	std::cout << "Would you like to save your mad lib (Y/N)?" << std::endl;
do
{
	std::cin >> save;

	if (save == 'y' || save == 'Y')
	{
		std::string path = "C:\\SoftwareDev\\madlib.txt";
		std::ofstream ofs(path);
		printToConsole(conIO, ofs);
		ofs.close();
		std::cout << "Your mad lib has been saved to C:\\SoftwareDev\\madlib.txt." << std::endl;;
	}
	else if (save == 'n' || save == 'N') { std::cout << "Your mad lib will not be saved." << std::endl; }
	else if (save != 'y' && save != 'Y' && save != 'n' && save != 'N') { std::cout << "Please enter a \"Y\" or \"N\" only!" << std::endl; }

} while (save != 'y' && save != 'Y' && save != 'n' && save != 'N');


	_getch();
	return 0;
}

void printToConsole(std::string *conIO, std::ostream &os)
{
	os << std::endl << "On July " << conIO[0] << " 1969, two American " << conIO[1]
	<< " were the first to " << conIO[2] << " on the moon. This " << conIO[3]
	<< " trip took " << conIO[4] << " days to reach from " << conIO[5]
	<< ". As " << conIO[6] << " Armstrong and " << conIO[7]
	<< " Aldrin " << conIO[8] << " onto the " << conIO[9]
	<< " of the moon, Armstrong " << conIO[10]
	<< " the famous words, \"That\'s " << conIO[11] << " " << conIO[12] << " "
	<< " step for man, & one " << conIO[13] << " leap for mankind\"."
	<< " Soon after, Aldrin " << conIO[14] << " onto the moon"
	<< " and together, they planted a U.S. " << conIO[15] << " on the surface. They "
	<< conIO[16] << " from the moon\'s surface to head"
	<< " back to earth and " << conIO[17] << " returned home " << conIO[18] << " days later." << std::endl << std::endl;
}